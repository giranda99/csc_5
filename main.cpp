/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 24, 2014, 3:28 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;
//std is short for standard
//also defines contexts of new words
/*//Cout means console out
 *///all statements end in semi colon
// << is the stream operator
//white blank lines are ignored 
//"Hello World" is a string literal
//end l makes a new line
//return 0: if code gets here then it executed correctly
//return 0 is a return flag that says everything went OK
//all code is inside curly brackets
int main(int argc, char** argv) {
    //variable definition
    //variable name is message
    //Assigning the world "Hello World" to message
    //output the variable "message"
    string message; //variable decleation
    //message = "Hello World"; //Variable initionalization (Variable can only have one intioonaltion)
    //Cin gets input from console
    //Prompt the user
    cout << "Enter a cheese" << endl;
    cin >> message;          
    return 0;
}

